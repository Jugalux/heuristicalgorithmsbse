import java.util.Arrays;
import java.util.Random;
import java.time.*;

public class Main {

    final static int SIZE = 127;
    final int MAX_ITERATIONS = 150;
    final int TABU_TENURE = 25;
    //Point[] p_init = new Point[SIZE];
    //Génération aléatoire des points
    int[] bestPath;

    public static void main(String[] args) {
        Main programme = new Main();
        Random rand = new Random();
        Point[] p_init = new Point[SIZE];
        for (int i = 0; i < SIZE; i++) {
            p_init[i] = new Point(rand.nextInt(350,1080), rand.nextInt(720));
            System.out.print("(" + p_init[i].x + ", " + p_init[i].y + "), ");
        }
      
        int k = 0;
        while(k < 10)
        {
            System.out.println("\n\n Iteration "+(k+1));
            long inst1 = System.currentTimeMillis();
            programme.executeTabuSearch(p_init);
            long inst2 = System.currentTimeMillis();      
            System.out.println("Elapsed Time in milliseconds : "+ ((inst2-inst1)));
            k++;
        }
    }

    void executeTabuSearch(Point[] p_init) {
        Random rand = new Random();
        int[][] graph = new int[SIZE][SIZE];
        Point[] p = p_init;
        int[] bestSolution = new int[SIZE];
        int[] currentSolution = new int[SIZE];
        TabuList tabuList = new TabuList(TABU_TENURE);

        //Génération aléatoire des points
        //for (int i = 0; i < SIZE; i++) {
        //    p[i] = new Point(rand.nextInt(350,1080), rand.nextInt(720));
        //    System.out.println("Ville " + i + ": (" + p[i].x + ", " + p[i].y + ")");
        //}



        //System.out.println("\nMatrice des distances :");
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                graph[i][j] = euclidDistance(p[i], p[j]);
        //        System.out.print(graph[i][j] + "\t");
            }
        //    System.out.println();
        }

        // Initialisation de la solution initiale avec la ville de départ fixée
        for (int i = 0; i < SIZE; i++) {
            bestSolution[i] = (i) % SIZE;
        }

        // Initialisation du meilleur chemin avec la solution de départ
        bestPath = Arrays.copyOf(bestSolution, bestSolution.length);

        // Calcul du coût de la solution de départ
        int coutDepart = pathOfCost(graph, bestSolution);
        //System.out.println("\nSolution de départ - Coût : " + coutDepart);
        //displayPath(bestSolution, graph);

        // Recherche tabou
        for (int iteration = 0; iteration < MAX_ITERATIONS; iteration++) {
            // Générer une solution voisine
            generateNeighbor(bestSolution, currentSolution, tabuList, rand);

            // Calculer le coût de la solution actuelle
            int currentCost = pathOfCost(graph, currentSolution);

            // Mettre à jour la meilleure solution si nécessaire
            if (currentCost < pathOfCost(graph, bestPath)) {
                bestPath = Arrays.copyOf(currentSolution, currentSolution.length);
            }

            //System.out.println("\nItération " + (iteration + 1) + " - Coût actuel : " + currentCost);
            //displayPath(currentSolution, graph);
        }

        System.out.println("\n*************************************************** TABU SEARCH *******************************************************");
        System.out.println("\nMeilleur coût final : " + pathOfCost(graph, bestPath));
        //displayBestPath(graph);
        System.out.println("\n*************************************************** TABU SEARCH *******************************************************\n\n");
    
    }

    int euclidDistance(Point p1, Point p2) {
        return (int) Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    int pathOfCost(int[][] g, int[] S) {
        int cout = 0;
        for (int i = 0; i < S.length; i++) {
            cout += g[S[i]][S[(i + 1) % S.length]];
        }
        return cout;
    }

    void generateNeighbor(int[] bestSolution, int[] currentSolution, TabuList tabuList, Random rand) {
        // Copier la meilleure solution dans la solution actuelle
        System.arraycopy(bestSolution, 0, currentSolution, 0, bestSolution.length);

        boolean tabuMove = true;

        while (tabuMove) {
            // Explorer des voisinages plus intelligents, comme l'échange de séquences de villes (éviter la ville de départ)
            int index1 = rand.nextInt(SIZE - 1) + 1;
            int index2 = rand.nextInt(SIZE - 1) + 1;

            // Vérifier si le mouvement est tabou
            if (!tabuList.isTabu(index1, index2)) {
                // Si le mouvement n'est pas tabou, le réaliser
                int temp = currentSolution[index1];
                currentSolution[index1] = currentSolution[index2];
                currentSolution[index2] = temp;

                // Sortir de la boucle
                tabuMove = false;
            }
        }
    }


    void displayPath(int[] solution, int[][] graphe) {
        int cout = pathOfCost(graphe, solution);
        /*for (int j : solution) {
            System.out.print(j + " --> ");
        }
        System.out.println(solution[0] + " (Coût : " + cout + ")");*/
    }

    void displayBestPath(int[][] graphe) {
        /*System.out.print("Meilleur chemin trouvé : ");
        displayPath(bestPath, graphe);*/
    }
}
