import java.util.ArrayList;

public class TabuList {
    private final ArrayList<int[]> list;
    private final int tenure;

    public TabuList(int tenure) {
        this.tenure = tenure;
        list = new ArrayList<>();
    }

    public void add(int index1, int index2) {
        list.add(new int[]{index1, index2});
        if (list.size() > tenure) {
            list.remove(0);
        }
    }

    public boolean isTabu(int index1, int index2) {
        for (int[] move : list) {
            if (move[0] == index1 && move[1] == index2) {
                return true;
            }
        }
        return false;
    }
}
