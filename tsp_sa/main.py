import time

import numpy
import cv2


def generate(width, height, count):
    cities = []
    for i in range(count):
        position_x = numpy.random.randint(350,width)
        position_y = numpy.random.randint(height)
        cities.append((position_x, position_y))
    return cities


def initialize(count):
    solution = numpy.arange(count)
    numpy.random.shuffle(solution)
    return solution


def evaluate(cities, solution):
    distance = 0
    for i in range(len(cities)):
        index_a = solution[i]
        index_b = solution[i - 1]
        delta_x = cities[index_a][0] - cities[index_b][0]
        delta_y = cities[index_a][1] - cities[index_b][1]
        distance += (delta_x ** 2 + delta_y ** 2) ** 0.5
    return distance


def modify(current):
    new = current.copy()
    index_a = numpy.random.randint(len(current))
    index_b = numpy.random.randint(len(current))
    while index_b == index_a:
        index_b = numpy.random.randint(len(current))
    new[index_a], new[index_b] = new[index_b], new[index_a]
    return new


def draw(width, height, cities, solution, infos):
    frame = numpy.zeros((height, width, 3))
    for i in range(len(cities)):
        index_a = solution[i]
        index_b = solution[i - 1]
        point_a = (cities[index_a][0], cities[index_a][1])
        point_b = (cities[index_b][0], cities[index_b][1])
        cv2.line(frame, point_a, point_b, GREEN, 2)
    for city in cities:
        cv2.circle(frame, (city[0], city[1]), 5, RED, -1)
    cv2.putText(frame, f"Temperature", (25, 50), FONT, SIZE, WHITE)
    cv2.putText(frame, f"Score", (25, 75), FONT, SIZE, WHITE)
    cv2.putText(frame, f"Best Score", (25, 100), FONT, SIZE, WHITE)
    cv2.putText(frame, f"Worst Score", (25, 125), FONT, SIZE, WHITE)
    cv2.putText(frame, f": {infos[0]:.2f}", (175, 50), FONT, SIZE, WHITE)
    cv2.putText(frame, f": {infos[1]:.2f}", (175, 75), FONT, SIZE, WHITE)
    cv2.putText(frame, f": {infos[2]:.2f}", (175, 100), FONT, SIZE, WHITE)
    cv2.putText(frame, f": {infos[3]:.2f}", (175, 125), FONT, SIZE, WHITE)
    cv2.imshow("Simulated Annealing", frame)
    cv2.waitKey(5)


WIDTH = 1080
HEIGHT = 720
CITY_COUNT = 127
INITIAL_TEMPERATURE = 1000
STOPPING_TEMPERATURE = 1
COOLING_COEFF = 0.95
FONT = cv2.FONT_HERSHEY_DUPLEX
SIZE = 0.7
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (0, 0, 255)

if __name__ == "__main__":
    #cities = generate(WIDTH, HEIGHT, CITY_COUNT)
    #cities = [(1079, 385), (1014, 209), (813, 164), (823, 691), (441, 30), (644, 314), (899, 500), (464, 466), (998, 77), (434, 183), (656, 240), (470, 42), (554, 679), (696, 336), (1015, 611), (655, 314), (778, 5), (950, 322), (993, 687), (542, 400), (1056, 619), (953, 87), (556, 65), (430, 699), (763, 226), (416, 710), (730, 259), (577, 347), (697, 255), (734, 539), (593, 327), (658, 572), (1055, 372), (375, 576), (845, 285), (415, 147), (670, 631), (425, 659), (555, 78), (727, 24), (606, 688), (837, 392), (502, 306), (624, 28), (832, 263), (555, 707), (725, 201), (916, 66), (914, 73), (857, 673), (723, 590), (748, 260), (522, 275), (759, 691), (748, 677), (498, 334), (641, 374), (665, 691), (613, 326), (822, 181), (394, 325), (455, 678), (967, 440), (1011, 19), (374, 437), (540, 303), (814, 647), (919, 442), (687, 98), (707, 591), (993, 509), (703, 425), (564, 173), (743, 622), (606, 282), (370, 520), (558, 114), (509, 610), (802, 418), (375, 628), (979, 706), (974, 167), (537, 151), (574, 276), (578, 255), (510, 613), (477, 149), (456, 506), (1063, 191), (536, 583), (646, 174), (475, 618), (830, 480), (981, 92), (452, 385), (361, 425), (892, 306), (871, 683), (883, 415), (541, 663), (987, 329), (700, 514), (882, 656), (751, 626), (1066, 513), (415, 259), (420, 492), (468, 606), (749, 41), (366, 40), (362, 430), (680, 143), (525, 28), (548, 286), (731, 501), (990, 295), (1044, 658), (403, 357), (383, 214), (677, 33), (533, 525), (646, 73), (1050, 460), (743, 671), (696, 556), (718, 681), (362, 415)]
    #cities = [(558, 269), (1072, 202), (945, 197), (685, 322), (493, 53), (377, 181), (788, 450), (962, 465), (830, 529), (799, 417), (396, 243), (941, 623), (1072, 280), (463, 611), (394, 353), (768, 237), (988, 534), (522, 459), (424, 385), (673, 7), (893, 645), (505, 33), (951, 366), (517, 116), (840, 534), (704, 231), (505, 673), (557, 498), (1060, 390), (403, 347), (730, 507), (699, 508), (589, 76), (765, 387), (630, 103), (608, 550), (906, 135), (822, 384), (665, 186), (884, 285), (1073, 497), (924, 46), (958, 245), (477, 441), (587, 400), (554, 32), (444, 223), (592, 508), (1060, 2), (591, 37), (708, 44), (391, 433), (840, 302), (834, 280), (627, 603), (580, 144), (923, 631), (817, 41), (856, 641), (575, 502), (363, 490), (1017, 694), (963, 0), (615, 623), (739, 492), (820, 103), (1078, 212), (1042, 142), (394, 158), (1020, 459), (543, 563), (986, 625), (866, 411), (761, 717), (812, 554), (747, 479), (357, 182), (966, 275), (668, 660), (1021, 139), (689, 511), (1008, 346), (609, 340), (897, 96), (378, 620), (447, 701), (736, 55), (606, 356), (616, 273), (688, 446), (1029, 484), (696, 583), (454, 458), (938, 345), (872, 29), (954, 434), (698, 350), (525, 5), (417, 462), (1047, 539), (437, 491), (655, 627), (1014, 346), (979, 445), (653, 302), (425, 367), (564, 147), (489, 706), (838, 634), (598, 172), (928, 497), (918, 170), (388, 429), (419, 58), (910, 71), (733, 131), (531, 512), (947, 65), (708, 514), (937, 177), (1003, 405), (641, 597), (688, 347), (1036, 372), (416, 505), (626, 305), (829, 434)]
    cities = [(737, 250), (984, 221), (712, 598), (1030, 414), (748, 503), (624, 180), (921, 571), (363, 538), (557, 341), (768, 616), (424, 646), (604, 654), (848, 372), (655, 202), (843, 121), (806, 551), (638, 251), (714, 626), (468, 403), (433, 201), (481, 653), (813, 227), (359, 571), (632, 598), (450, 133), (803, 201), (485, 705), (1056, 262), (697, 347), (854, 219), (1067, 449), (922, 142), (508, 526), (675, 715), (664, 305), (1049, 395), (620, 153), (628, 274), (1079, 463), (820, 485), (561, 384), (1003, 103), (780, 469), (588, 623), (936, 97), (642, 423), (686, 215), (556, 355), (955, 374), (892, 459), (523, 226), (826, 167), (661, 719), (910, 135), (694, 220), (746, 204), (601, 10), (1055, 438), (504, 168), (522, 555), (1066, 381), (572, 241), (572, 592), (536, 568), (466, 471), (797, 647), (1077, 603), (907, 305), (753, 388), (363, 228), (649, 662), (969, 510), (468, 216), (590, 411), (1054, 221), (975, 480), (729, 448), (372, 670), (856, 417), (778, 99), (399, 182), (373, 322), (797, 400), (594, 13), (598, 316), (897, 669), (1001, 353), (531, 671), (957, 477), (748, 602), (725, 231), (894, 336), (549, 320), (809, 289), (694, 508), (545, 551), (608, 214), (754, 658), (350, 629), (465, 16), (566, 601), (793, 135), (617, 353), (610, 291), (388, 349), (423, 311), (1007, 203), (813, 289), (1062, 17), (819, 383), (545, 134), (810, 152), (734, 28), (596, 505), (533, 589), (743, 476), (485, 482), (736, 214), (544, 298), (464, 206), (1066, 573), (728, 53), (431, 494), (762, 294), (846, 662), (875, 604), (854, 370)]

    print("\nCities are :  \n", cities)
    count = 1
    list_best = []
    list_worst = []
    list_time = []
    while count <= 10:
        print("\nIteration :", count)
        start_time = time.time()
        current_solution = initialize(CITY_COUNT)
        current_score = evaluate(cities, current_solution)
        best_score = worst_score = current_score
        temperature = INITIAL_TEMPERATURE
        while temperature > STOPPING_TEMPERATURE:
            new_solution = modify(current_solution)
            new_score = evaluate(cities, new_solution)
            best_score = min(best_score, new_score)
            worst_score = max(worst_score, new_score)
            if new_score < current_score:
                current_solution = new_solution
                current_score = new_score
            else:
                delta = new_score - current_score
                probability = numpy.exp(-delta / temperature)
                if probability > numpy.random.uniform():
                    current_solution = new_solution
                    current_score = new_score
            temperature *= COOLING_COEFF
            infos = (temperature, current_score, best_score, worst_score)
            draw(WIDTH, HEIGHT, cities, current_solution, infos)
        print("\nBest score", best_score)
        print("\nWorst score", worst_score)
        list_best.append(best_score)
        list_worst.append(worst_score)
        end_time = time.time()
        # display computation time
        print('\nTotal time:\t%.3f sec' % (end_time - start_time))
        list_time.append(end_time - start_time)
        count += 1

    print(list_time)
    print(list_best)
    print(list_worst)
    print(list_time)
    print("Average score : ", (sum(list_best)/len(list_best)))
    print("Average time : ", (sum(list_time) / len(list_time)))


    #i = 1
    #while i > 0:
    #    i += 1
    #    infos = (temperature, current_score, best_score, worst_score)
    #    draw(WIDTH, HEIGHT, cities, current_solution, infos)
