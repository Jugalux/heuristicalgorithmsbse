import random
import math

def distance(city1, city2):
    """Calcule la distance euclidienne entre deux villes"""
    x1, y1 = city1
    x2, y2 = city2
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

def calculate_tour_length(tour, cities):
    """Calcule la longueur totale d'une tournée"""
    total_length = 0
    num_cities = len(tour)

    for i in range(num_cities):
        city1 = cities[tour[i]]
        city2 = cities[tour[(i + 1) % num_cities]]
        total_length += distance(city1, city2)

    return total_length

def ils_tsp(cities, max_iterations):
    """Fonction Iterated Local Search (ILS) pour le TSP"""
    num_cities = len(cities)
    
    # Initialisation: Générer la solution initiale avec une permutation aléatoire des villes
    best_tour = random.sample(range(num_cities), num_cities)
    best_length = calculate_tour_length(best_tour, cities)

    for _ in range(max_iterations):
        # Perturbation : Échange aléatoire de deux villes
        tour = best_tour.copy()
        i, j = random.sample(range(num_cities), 2)
        tour[i], tour[j] = tour[j], tour[i]

        # Recherche locale en utilisant la fonction local_search
        improved_tour = local_search(tour, cities)

        # Acceptation si la nouvelle tournée est meilleure
        length = calculate_tour_length(improved_tour, cities)
        if length < best_length:
            best_tour = improved_tour
            best_length = length

    return best_tour, best_length

def local_search(tour, cities):
    """Fonction local_search pour l'optimisation de la recherche locale en effectuant des échanges locaux pour améliorer la solution"""
    num_cities = len(tour)
    while True:  # Boucle indéfiniment jusqu'à ce qu'aucune amélioration ne soit trouvée
        improved = False
        for i in range(1, num_cities - 2):
            for j in range(i + 1, num_cities):
                if j - i == 1:
                    continue  # Les arêtes consécutives ne peuvent pas être inversées
                new_tour = tour[:i] + tour[i:j][::-1] + tour[j:] # La première sous-liste correspond aux villes avant l'indice i, la deuxième sous-liste correspond aux villes entre les indices i et j inversées, et la troisième sous-liste correspond aux villes après l'indice j
                new_length = calculate_tour_length(new_tour, cities)
                if new_length < calculate_tour_length(tour, cities):
                    tour = new_tour
                    improved = True
        if not improved:  # Si aucune amélioration n'est trouvée, la boucle s'arrête
            break
    return tour

# Exemple d'utilisation
cities = [(737, 250), (984, 221), (712, 598), (1030, 414), (748, 503), (624, 180), (921, 571), (363, 538), (557, 341), (768, 616), (424, 646), (604, 654), (848, 372), (655, 202), (843, 121), (806, 551), (638, 251), (714, 626), (468, 403), (433, 201), (481, 653), (813, 227), (359, 571), (632, 598), (450, 133), (803, 201), (485, 705), (1056, 262), (697, 347), (854, 219)]
max_iterations = 200

best_tour, best_length = ils_tsp(cities, max_iterations)

print("Meilleure solution(chemin): ", best_tour)
print("Longueur(Coût) de la meilleure solution: ", best_length)

